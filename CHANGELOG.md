## 2.0.0
- 包管理工具由npm切换为ohpm
- 适配DevEco Studio: 3.1Beta2(3.1.0.400)
- 适配SDK: API9 Release(3.2.11.9)

## 1.0.5
- 适配DevEco Studio 3.1 Beta1版本
- 调整XComponent用法

## 1.0.2
- 添加支持直播rtsp协议

## 1.0.1
- 修复getMediaInfo()接口crash问题

## 1.0.0
- 发布ijkplayer项目
- 支持直播、点播 (直播支持hls、rtmp协议) 
- 支持播放、暂停、停止、重置、释放
- 支持快进、后退
- 支持倍数播放
- 支持循环播放
- 支持设置屏幕常亮
- 支持设置音量
- 支持FFmpeg软解码  
- 不支持硬解码

